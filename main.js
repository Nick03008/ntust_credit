var library = {
    js:[
        "https://code.jquery.com/jquery-3.2.1.js",
        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    ],
    css:[
        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
    ]
}

library.css.forEach(function(url){
    var sc = document.createElement("link");
    sc.setAttribute("href", url);
    sc.setAttribute("rel", "stylesheet");
    document.head.appendChild(sc);
})

library.js.forEach(function(url,index){
    var sc = document.createElement("script");
    sc.onload(function(
        console.log(index);
    ))
    sc.setAttribute("src", url);
    sc.setAttribute("type", "text/javascript");
    var b = document.head.appendChild(sc);
})



